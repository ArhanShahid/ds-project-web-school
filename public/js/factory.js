webSchool.factory('articleFactory', function($http) {
    return {
        getArticleAsync: function(callback) {
            $http.get('js/json/webSchool.json').success(callback);
        }
    };
});

webSchool.factory('randomNumberFactory',function(){
    var arr = [];
    for(var i=0 ; i<10; i++){
        var temp = Math.floor(Math.random()*10);
        arr.push(temp)
    }
    return  arr;
});

webSchool.factory('randomString',function(){
    var arr = ['Cherry','Blackberry','Melon','Lemon','Mango','Orange','Apples','Lychee','Peach'];
    return  arr;
})

