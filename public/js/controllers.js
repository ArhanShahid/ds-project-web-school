webSchool.controller('articleController', function($scope, articleFactory) {
    articleFactory.getArticleAsync(function(results) {
        console.log('Working :) ');
        $scope.articles = results.articles;
    });
});
webSchool.controller('bubbleSortController',function($scope,randomNumberFactory){

    $scope.Unsorted = randomNumberFactory;
    function BubbleSort(arr){
        for(var i=0; i < arr.length; i++){
            for(var j = 0; j < arr.length; j++){
                if (arr[i] < arr[j])
                {
                    var temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }
        return arr;
    }
    var tempArr = randomNumberFactory.slice();
    $scope.Sorted = BubbleSort(tempArr);
});
webSchool.controller('insertionSortController',function($scope,randomNumberFactory){

    $scope.Unsorted = randomNumberFactory;

    function InsertionSort(arr){
        for(var i=1;i < arr.length;i++){
            var key = arr[i];
            var j = i-1;
            while(j >= 0 && arr[j] > key){
                arr[j + 1] = arr[j];
                j--;
            }
            arr[j + 1] = key;
        }
        return arr;
    }

    var tempArr = randomNumberFactory.slice();
    $scope.Sorted = InsertionSort(tempArr);
});
webSchool.controller('selectionSortController',function($scope,randomNumberFactory){

    $scope.Unsorted = randomNumberFactory;

    function SelectionSort(arr){
        var i, j, min, temp;
        for (i = 0; i < arr.length - 1; i++)
        {
            min = i;
            for (j = i + 1; j < arr.length; j++)
            {
                if (arr[j] < arr[min])
                {
                    min = j;
                }
            }
            temp = arr[i];
            arr[i] = arr[min];
            arr[min] = temp;
        }
        return arr;
    }

    var tempArr = randomNumberFactory.slice();
    $scope.Sorted = SelectionSort(tempArr);
});
webSchool.controller('quickSortController',function($scope,randomNumberFactory){

    $scope.Unsorted = randomNumberFactory;

    function  QuickSort(arr, left, right) {
        var i = left;
        var j = right;
        var tmp;
        var pivotidx = (left + right) / 2;
        var pivot = parseInt(arr[pivotidx.toFixed()]);
        /* partition */
        while (i <= j) {
            while (parseInt(arr[i]) < pivot)
                i++;
            while (parseInt(arr[j]) > pivot)
                j--;
            if (i <= j) {
                tmp = arr[i];
                arr[i] = arr[j];
                arr[j] = tmp;
                i++;
                j--;
            }
        }
        /* recursion */
        if (left < j)
            QuickSort(arr, left, j);
        if (i < right)
            QuickSort(arr, i, right);
        return arr;
    }

    var tempArr = randomNumberFactory.slice();

    $scope.Sorted = QuickSort(tempArr,0,tempArr.length-1);
});
webSchool.controller('linearSearchController',function($scope,randomString){

    $scope.stringPrint = randomString;
    $scope.searchName = null;
    $scope.searchStatus = null;
    var InFound = true;
    $scope.linearSearchFunction =  function(){
        for(var i=0;i < randomString.length; i++){

            if(randomString[i] == $scope.searchName){
                $scope.searchStatus = "Found At "+ (++i) +" Index";
                InFound = false;
            }
        }
        if(InFound){
            $scope.searchStatus = "Not Found";
        }
    }

});
webSchool.controller('binarySearchController',function($scope,randomNumberFactory){

    var UnsortedArray = randomNumberFactory.slice();
    var SortedArray = UnsortedArray.sort();
    $scope.stringPrint = SortedArray;
    $scope.searchName = null;
    $scope.searchStatus = null;

    $scope.binarySearchFunction =  function(){

       var value = $scope.searchName;
        function binarySearch(items, value){
            var startIndex  = 0,
                stopIndex   = items.length - 1,
                middle      = Math.floor((stopIndex + startIndex)/2);
            while(items[middle] != value && startIndex < stopIndex){
                if (value < items[middle]){
                    stopIndex = middle - 1;
                } else if (value > items[middle]){
                    startIndex = middle + 1;
                }
                //recalculate middle
                middle = Math.floor((stopIndex + startIndex)/2);
            }
            //make sure it's the right value
            return (items[middle] != value) ? -1 : middle;
        }
        var binarySearchResult = binarySearch(SortedArray, value);
        if(binarySearchResult == -1){
            $scope.searchStatus = "Not Found";
        }else{
            $scope.searchStatus = "Found at "+(++binarySearchResult)+" Index";
        }
     }
});
webSchool.controller('fibonacciSequenceController',function($scope){

    $scope.fibonacciSequenceInput = null;
    var arr = [];
    $scope.fibonacciSequenceFunction =  function(){

        var a=0, b=1, c, i;

        for (i = 1; i <= $scope.fibonacciSequenceInput; i++)
        {
          c = a + b;
          a = b;
          b = c;
          arr.push(c);
         }

        console.log(arr);
       $scope.fibonacciSequence = arr;
    }

});
webSchool.controller('recursionController',function($scope){

    $scope.getFactorialInput = null;
    $scope.Factorial = null;

    $scope.getFactorialFunction =  function(){

       var input = $scope.getFactorialInput;
       function Factorial(input){
           if (input == 0)
           {
               return 1;
           }
           return input * Factorial(input - 1);
       }

        $scope.Factorial = Factorial(input);
    }

});
webSchool.controller('linkedListController',function($scope){

    $scope.addElement = null;
    $scope.removeName = null;

    var LinkedList = function(){

        var that = {}, first, last;

        that.push = function(value){
            var node = new Node(value);
            if(first == null){
                first = last = node;
            }else{
                last.next = node;
                last = node;
            }
        };

        that.pop = function(){
            var value = first;
            first = first.next;
            return value;
        };

        that.remove = function(index) {
            var i = 0;
            var current = first, previous;
            if(index === 0){
                //handle special case - first node
                first = current.next;
            }else{
                while(i++ < index){
                    //set previous to first node
                    previous = current;
                    //set current to the next one
                    current = current.next
                }
                //skip to the next node
                previous.next = current.next;
            }
            return current.value;
        };

        var Node = function(value){
            this.value = value;
            var next = {};
        };

        return that;
    };
    var linkedList = new LinkedList();
//    linkedList.push(1);
//    linkedList.push(2);
//    linkedList.push(3);
//    linkedList.push(4);
    $scope.pushElementFunction =  function(){

        var value = $scope.addElement;
//        $scope.elementPushed = value +" Pushed";
        $scope.element = value +" Pushed";
        linkedList.push(value);

    }

    $scope.removeElementFunction =  function(){

        var value = $scope.removeName;
//        $scope.elementRemoved ="Element on "+ value +"Position Removed";
        $scope.element = "Element on "+ value +" Position Removed";
        linkedList.remove(value);

    }
    $scope.popElementFunction =  function(){

        var x = linkedList.pop();
//        $scope.popValue = x.value + " Poped";
        $scope.element = x.value + " Poped";
        console.log(x.value);

    }
//    console.log(linkedList.pop());
//    console.log(linkedList.pop());
//    console.log(linkedList.pop());

});
webSchool.controller('stackController',function($scope){


    $scope.pushElement = null;

    function Stack() {
        this.dataStore = new Array(5);
        this.top = 0;
        this.push = push;
        this.pop = pop;
        this.peek = peek;
        this.clear = clear;
        this.length = length;
    }


    function push(element) {
        this.dataStore[this.top++] = element;
    }
    function peek() {
        return this.dataStore[this.top-1];
    }
    function pop() {
        return this.dataStore[--this.top];
    }
    function clear() {
        this.top = 0;
    }
    function length() {
        return this.top;
    }

    var s = new Stack();

    $scope.StackPush = function(){
        var Input = $scope.pushElement;
        s.push(Input);
        $scope.element = Input + " Pushed";
    }
    $scope.StackLength = function(){
        $scope.element = "length: " + s.length();
    }
    $scope.StackPeek = function(){
        $scope.element = "Peek Value : "+s.peek();
    }
    $scope.StackPop = function(){
        var popped = s.pop();
        $scope.element = popped+ " Popped";
    }
    $scope.StackClear = function(){
        s.clear();
    }

});
webSchool.controller('queueController',function($scope){

            $scope.enqueueElement = null;

            function Queue() {
                this.dataStore = [];
                this.enqueue = enqueue;
                this.dequeue = dequeue;
                this.front = front;
                this.back = back;
                this.toString = toString;
                this.empty = empty;
            }
            function enqueue(element) {
                this.dataStore.push(element);
            }
            function dequeue() {

                return this.dataStore.shift();
            }
            function front() {
                return this.dataStore[0];
            }
            function back() {
                return this.dataStore[this.dataStore.length-1];
            }
            function toString() {
                var retStr = "";
                for (var i = 0; i < this.dataStore.length; ++i) {
                    retStr += this.dataStore[i] + "\n";
                }
                return retStr;
            }
            function empty() {
                if (this.dataStore.length == 0) {
                    return true;
                }
                else {
                    return false;
                }
            }

            var q = new Queue();

            $scope.EnqueueFunction = function(){
                var Input = $scope.enqueueElement;
                $scope.element = "Enqueue : "+Input;
                q.enqueue(Input);
            }
            $scope.DequeueFunction = function(){
                q.dequeue();
            }
            $scope.Front =  function(){
                $scope.element = "Front : " + q.front();
            }
            $scope.Back = function(){
                $scope.element ="Back : " + q.back();
            }
            $scope.Empty = function (){
                alert("Empty")
                q.empty();
            }


//
//            q.enqueue("Meredith");
//            q.enqueue("Cynthia");
//            q.enqueue("Jennifer");
        //        alert(q.toString());
        //        q.dequeue();
        //        alert(q.toString());
        //        alert("Front of queue: " + q.front());
        //        alert("Back of queue: " + q.back());



});
