
var webSchool = angular.module('webSchool',['ngRoute']);
    webSchool.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.when('/',{
            templateUrl:'partials/home.html'
        }).
            when('/ds',{
                templateUrl:'partials/ds.html'
            }).
            when('/article',{
                templateUrl:'partials/article.html',
                controller:'articleController'
            }).
            when('/contact',{
                templateUrl:'partials/contact.html'
            }).
            when('/bubbleSort',{
                templateUrl:'partials/algorithmPages/bubbleSort.html',
                controller:'bubbleSortController'
            }).
            when('/insertionSort',{
                templateUrl:'partials/algorithmPages/insertionSort.html',
                controller:'insertionSortController'
            }).
            when('/selectionSort',{
                templateUrl:'partials/algorithmPages/selectionSort.html',
                controller:'selectionSortController'
            }).
            when('/quickSort',{
                templateUrl:'partials/algorithmPages/quickSort.html',
                controller:'quickSortController'
            }).
            when('/linearSearch',{
                templateUrl:'partials/algorithmPages/linearSearch.html',
                controller:'linearSearchController'
            }).
            when('/binarySearch',{
                templateUrl:'partials/algorithmPages/binarySearch.html',
                controller:'binarySearchController'
            }).
            when('/fibonacciSequence',{
                templateUrl:'partials/algorithmPages/fibonacciSequence.html',
                controller:'fibonacciSequenceController'
            }).
            when('/recursion',{
                templateUrl:'partials/algorithmPages/recursion.html',
                controller:'recursionController'
            }).
            when('/linkedList',{
                templateUrl:'partials/algorithmPages/linkedList.html',
                controller:'linkedListController'
            }).
            when('/stack',{
                templateUrl:'partials/algorithmPages/stack.html',
                controller:'stackController'
            }).
            when('/queue',{
                templateUrl:'partials/algorithmPages/queue.html',
                controller:'queueController'
            }).
            otherwise({
                redirectTo:'/'
            });

    }]);





